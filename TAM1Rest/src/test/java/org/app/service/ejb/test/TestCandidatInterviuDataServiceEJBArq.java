package org.app.service.ejb.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.InterviuCandidat;
import org.app.service.ejb.InterviuCandidatEJB;
//import org.app.service.ejb.ProjectFactory;
import org.app.service.ejb.CandidatInterviuService;
import org.app.service.ejb.CandidatInterviuServiceEJB;
//import org.app.service.ejb.ValidatorInterceptor;
import org.app.service.entities.Candidat;
import org.app.service.entities.Interviu;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

/*
 * JUnit test for EJB: TestProjectReleaseFeatureDataServiceEJBArq
 */
@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCandidatInterviuDataServiceEJBArq  {
private static Logger logger = Logger.getLogger(TestCandidatInterviuDataServiceEJBArq.class.getName());
//Arquilian infrastructure
	@Deployment
 public static Archive<?> createDeployment() {
     return ShrinkWrap
             .create(WebArchive.class, "scrum-test-ejb.war")
             .addPackage(EntityRepository.class.getPackage()).addPackage(Candidat.class.getPackage())
             .addClass(InterviuCandidat.class).addClass(InterviuCandidatEJB.class)
             .addClass(CandidatInterviuService.class).addClass(CandidatInterviuServiceEJB.class)
             .addAsResource("META-INF/persistence.xml")
             .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
 }
	@EJB // Test EJB Data Service Reference is injected
	private static CandidatInterviuService service;	
	// JUnit test methods
	@Test
	public void test4_GetCandidat() {
		logger.info("DEBUG: Junit TESTING: testGetCandidat 4 ...");
		Candidat candidat = service.getById(4);
		assertNotNull("Fail to Get Candidat 4!", candidat);
	}
	/* CREATE Test 2: create aggregate*/
	@Test
	public void test3_CreateNewCandidat(){
		Candidat candidat = service.createNewCandidat(4, "074 7123457","Candidat_101@yahoo.com");
		assertNotNull("Fail to create new candidat in repository!", candidat);
		// update project
		candidat.setNumeCandidat(candidat.getNumeCandidat() + " - changed by test client");		
		List<Interviu> interviuri = candidat.getInterviuri();
		for(Interviu I: interviuri)
			I.setNumeIntervievator(I.getNumeIntervievator() + " - changed by test client");
		candidat = service.add(candidat);
		assertNotNull("Fail to save new candidat in repository!", candidat);
		logger.info("DEBUG createNewCandidat: candidat changed: " + candidat);
		// check read
		candidat = service.getById(4);  // !!!
		assertNotNull("Fail to find changed candidat in repository!", candidat);
		logger.info("DEBUG createNewCandidat: queried candidat" + candidat);
	}		

	@Test
	public void test2_DeleteCandidat() {
		logger.info("DEBUG: Junit TESTING: testDeleteCandidat 4...");
		Candidat candidat = service.getById(4);  // !!!
		if (candidat != null)
			service.remove(candidat);
		candidat = service.getById(4);  // !!!
		assertNull("Fail to delete Candidat 4!", candidat);
	}	
	@Test
	public void test1_GetMessage() {
		logger.info("DEBUG: Junit TESTING: testGetMessage ...");
		String response = service.getMessage();
		assertNotNull("Data Service failed!", response);
		logger.info("DEBUG: EJB Response ..." + response);
	}	
}



	


