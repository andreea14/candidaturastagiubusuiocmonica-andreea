package org.app.service.ejb.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.service.ejb.IntrebareService;
import org.app.service.ejb.IntrebareServiceEJB;
import org.app.service.entities.Intrebare;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)


public class TestIntrebareDataServiceEJBArq {

	private static Logger logger = 
			Logger.getLogger(TestIntrebareDataServiceEJBArq.class.getName());
	
	@EJB // EJB DataService Ref
	private static IntrebareService service;
	
	@Deployment // Arquilian infrastructure
	public static Archive<?> createDeployment() {
	        return ShrinkWrap
	                .create(WebArchive.class, "SCRUM-S3-test.war")
	                .addPackage(Intrebare.class.getPackage())
	                .addClass(IntrebareService.class)
	                .addClass(IntrebareServiceEJB.class)
	                .addAsResource("META-INF/persistence.xml")
	                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	
	@Test
	public void test1_GetMessage() {
		logger.info("DEBUG: Junit TESTING: getMessage ...");
		String response = service.getMessage();
		assertNotNull("Data Service failed!", response);
		logger.info("DEBUG: EJB Response ..." + response);
	}

	@Test
	public void test4_GetIntrebari() {
		logger.info("DEBUG: Junit TESTING: testGetIntrebari ...");
		
		Collection<Intrebare> intrebari = service.getIntrebari();
		assertTrue("Fail to read intrebari!", intrebari.size() > 0);
	}

	@Test
	public void test3_AddIntrebare() {
		logger.info("DEBUG: Junit TESTING: testAddIntrebare ...");
		
		Integer intrebariToAdd = 3;
		for (int i=1; i <= intrebariToAdd; i++){
			service.addIntrebare(new Intrebare(100 + i, "Intrebare_" + (100 + i), i));
			//service.addIntrebare(new Intrebare(null, "Intrebare_" + (100 + i), i));
		}
		Collection<Intrebare> intrebari = service.getIntrebari();
		assertTrue("Fail to add intrebari!", intrebari.size() == intrebariToAdd);
	}

	@Test
	public void test2_DeleteIntrebare() {
		logger.info("DEBUG: Junit TESTING: testDeleteIntrebare ...");
		
		Collection<Intrebare> intrebari = service.getIntrebari();
		for (Intrebare i: intrebari)
			service.removeIntrebare(i);
		Collection<Intrebare> intrebariAfterDelete = service.getIntrebari();
		assertTrue("Fail to read intrebari!", intrebariAfterDelete.size() == 0);
	}	
}
/* http://localhost:8080/SCRUM-S2/data/features */