package org.app.service.ejb.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.CandidatService;
import org.app.service.ejb.CandidatServiceEJB;
import org.app.service.entities.Candidat;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TestCandidatDataServiceEJBArq {
private static Logger logger = Logger.getLogger(TestCandidatDataServiceEJBArq.class.getName());
	
	@EJB
	private static CandidatService service;
	
	@Deployment // Arquilian infrastructure
	public static Archive<?> createDeployment() {
	        return ShrinkWrap
	                .create(WebArchive.class, "SCRUM-S3-test.war")
	                .addPackage(Candidat.class.getPackage())
	                .addClass(CandidatService.class)
	                .addClass(CandidatServiceEJB.class)
	                .addClass(EntityRepository.class)
	                .addClass(EntityRepositoryBase.class)
	                .addAsResource("META-INF/persistence.xml")
	                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}	
	
	@Test
	public void test1_GetMessage() {
		logger.info("DEBUG: Junit TESTING: testGetMessage ...");
		String response = service.getMessage();
		assertNotNull("Data Service failed!", response);
		logger.info("DEBUG: EJB Response ..." + response);
	}

	@Test
	public void test4_GetCandidati() {
		logger.info("DEBUG: Junit TESTING: testGetCandidati ...");
		Collection<Candidat> Candidati= service.toCollection();
		assertTrue("Fail to read Candidati!", Candidati.size() > 0);
	}

	@Test
	public void test3_AddCandidat() {
		logger.info("DEBUG: Junit TESTING: testAddCandidat ...");
		
		Integer candidatiToAdd = 3;
		for (int i=1; i <= candidatiToAdd; i++){
			service.add(new Candidat(i, "Candidat_" + (100 + i), "074 " + (i+7123456), "Candidat_" +(100+1) + "@yahoo.com"));
		}
		Collection<Candidat> candidati = service.toCollection();
		assertTrue("Fail to add Candidati!", candidati.size() == candidatiToAdd);
	}

	@Test
	public void test2_DeleteCandidat() {
		logger.info("DEBUG: Junit TESTING: testDeleteCandidat ...");
		
		Collection<Candidat> candidati = service.toCollection();
		for (Candidat c: candidati)
			service.remove(c);
		Collection<Candidat> CandidatiAfterDelete = service.toCollection();
		assertTrue("Fail to read Candidati!", CandidatiAfterDelete.size() == 0);
	}	
}
	


