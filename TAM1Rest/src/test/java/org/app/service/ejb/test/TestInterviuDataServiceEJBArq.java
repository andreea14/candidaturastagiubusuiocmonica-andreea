package org.app.service.ejb.test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.service.ejb.InterviuCandidat;
import org.app.service.ejb.InterviuCandidatEJB;
import org.app.service.entities.Candidat;
import org.app.service.entities.Interviu;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TestInterviuDataServiceEJBArq {

		private static Logger logger = 
				Logger.getLogger(TestInterviuDataServiceEJBArq.class.getName());
		
		@EJB // EJB DataService Ref
		private static InterviuCandidat service;
		
		@Deployment // Arquilian infrastructure
		public static Archive<?> createDeployment() {
		        return ShrinkWrap
		                .create(WebArchive.class, "SCRUM-S3-test.war")
		                .addPackage(Interviu.class.getPackage())
		                .addClass(InterviuCandidat.class)
		                .addClass(InterviuCandidatEJB.class)
		                .addAsResource("META-INF/persistence.xml")
		                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		}

		private Candidat candidat;
		
		@Test
		public void test1_GetMessage() {
			logger.info("DEBUG: Junit TESTING: getMessage ...");
			String response = service.getMessage();
			assertNotNull("Data Service failed!", response);
			logger.info("DEBUG: EJB Response ..." + response);
		}

		@Test
		public void test4_GetInterviuri() {
			logger.info("DEBUG: Junit TESTING: testGetInterviuri ...");
			
			Collection<Interviu> interviuri = service.getInterviuri();
			assertTrue("Fail to read interviuri!", interviuri.size() > 0);
		}

		@Test
		public void test3_AddInterviu() {
			logger.info("DEBUG: Junit TESTING: testAddInterviu ...");
			Integer interviuriToAdd = 3;
			Candidat candidat= service.getCandidatById(2);
			for (int i=1; i <= interviuriToAdd; i++){
				service.addInterviu(new Interviu(200 + i, new Date(), "Intervievator" + i, "123" + i, candidat));
			}
			Collection<Interviu> interviuri = service.getInterviuri();
			assertTrue("Fail to add interviuri!", interviuri.size() == interviuriToAdd);
		}
		
		@Test
		public void test2_DeleteInterviu() {
			logger.info("DEBUG: Junit TESTING: testDeleteInterviu ...");
			
			Collection<Interviu> interviuri = service.getInterviuri();
			for (Interviu I: interviuri)
				service.removeInterviu(I);
			Collection<Interviu> interviuriAfterDelete = service.getInterviuri();
			assertTrue("Fail to read interviuri!", interviuriAfterDelete.size() == 0);
		}	
	}
	/* http://localhost:8080/SCRUM-S2/data/features */




