package org.app.service.ejb;


import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.app.service.entities.Intrebare;
//import javax.ws.rs.core.Application;

@Path("intrebari")
@Stateless @LocalBean

public class IntrebareServiceEJB implements IntrebareService {

private static Logger logger = Logger.getLogger(IntrebareServiceEJB.class.getName());
	
	/* DataService initialization */
	// Inject resource 
	@PersistenceContext(unitName="MSD")
	private EntityManager em;
	// Constructor
	public IntrebareServiceEJB() {
	}
	// Init after constructor
	@PostConstruct
	public void init(){
		logger.info("POSTCONSTRUCT-INIT : " + this.em);
	}		

	/* CRUD operations implementation */
	// CREATE or UPDATE
	@PUT @Path("/{IdIntrebare}")
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Override
	public Intrebare addIntrebare(Intrebare intrebareToAdd){
		em.persist(intrebareToAdd);
		em.flush();
		// transactions are managed by default by container
		em.refresh(intrebareToAdd);
		return intrebareToAdd;
	}	
	// READ
	@GET  @Path("/{IdIntrebare}")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Override
	public Intrebare getIntrebareByIdIntrebare(@PathParam ("IdIntrebare") Integer IdIntrebare) {
		logger.info("****DEBUG REST getIntrebareByIdIntrebare():IdIntrebare=" + IdIntrebare);
		return em.find(Intrebare.class, IdIntrebare);
	}	
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Collection<Intrebare> getIntrebari(){
		List<Intrebare> intrebari = em.createQuery("SELECT i FROM Intrebare i", Intrebare.class)
				.getResultList();
	logger.info("****DEBUG REST intrebari.size()=" + intrebari.size());
		return intrebari;
	}
	// REMOVE
	@DELETE
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Override
	public String removeIntrebare(Intrebare intrebareToDelete){
		intrebareToDelete = em.merge(intrebareToDelete);
		em.remove(intrebareToDelete);
		em.flush();
		return "True";
	}
	
	// Custom READ: custom query
	@GET @Path("/{ContinutIntrebare}")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Override
	public Intrebare getIntrebareByContinutIntrebare(@PathParam ("ContinutIntrebare") String ContinutIntrebare) {
		return em.createQuery("SELECT i FROM Intrebare i WHERE i.ContinutIntrebare = :ContinutIntrebare", Intrebare.class)
				.setParameter("ContinutIntrebare", ContinutIntrebare)
				.getSingleResult();
	}	
	
	// Others
	public String getMessage() {
		return "IntrebareServiceEJB is ON... ";
	}
}



