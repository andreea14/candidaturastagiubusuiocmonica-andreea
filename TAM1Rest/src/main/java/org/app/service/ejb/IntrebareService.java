package org.app.service.ejb;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remote;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.entities.EntityBase;
import org.app.service.entities.Intrebare;;

// Implement simple CRUD Operations
@Remote
public interface IntrebareService {

	// CREATE or UPDATE
		Intrebare addIntrebare(Intrebare intrebareToAdd);

		// DELETE
		String removeIntrebare(Intrebare intrebareToDelete);

		// READ
		Intrebare getIntrebareByIdIntrebare(Integer IdIntrebare);
		Collection<Intrebare> getIntrebari();
		
		// Custom READ: custom query
		Intrebare getIntrebareByContinutIntrebare(String intrebareContinutIntrebare);
		
		// Others
		String getMessage();
	}

