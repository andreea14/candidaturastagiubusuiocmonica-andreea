package org.app.service.ejb;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Application;
import org.app.service.entities.Test;

@Path("teste")
@Stateless @LocalBean
public class TestServiceEJB {

private static Logger logger = Logger.getLogger(TestServiceEJB.class.getName());
	
	/* DataService initialization */
	// Inject resource 
	@PersistenceContext(unitName="MSD")
	private EntityManager em;
	// Constructor
	public TestServiceEJB() {
	}
	// Init after constructor
	@PostConstruct
	public void init(){
		logger.info("POSTCONSTRUCT-INIT : " + this.em);
	}		

	/* CRUD operations implementation */
	// CREATE or UPDATE
	@PUT @Path("/{IdTest}")
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	//@Override
	public Test addTest(Test testToAdd){
		em.persist(testToAdd);
		em.flush();
		// transactions are managed by default by container
		em.refresh(testToAdd);
		return testToAdd;
	}	
	// READ
	@GET  @Path("/{IdTest}")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	//@Override
	public Test getTestByIdTest(@PathParam ("IdTest") Integer IdTest) {
		logger.info("****DEBUG REST getTestByIdTest():IdTest=" + IdTest);
		return em.find(Test.class, IdTest);
	}	
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Collection<Test> getTeste(){
		List<Test> teste = em.createQuery("SELECT t FROM Test t", Test.class)
				.getResultList();
	logger.info("****DEBUG REST teste.size()=" + teste.size());
		return teste;
	}
	// REMOVE
	@DELETE
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	//@Override
	public String removeTest(Test testToDelete){
		testToDelete = em.merge(testToDelete);
		em.remove(testToDelete);
		em.flush();
		return "True";
	}
	
	// Custom READ: custom query
	@GET @Path("/{NumeTest}")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	//@Override
	public Test getTestByNumeTest(@PathParam ("NumeTest") String NumeTest) {
		return em.createQuery("SELECT t FROM Test t WHERE t.NumeTest = :NumeTest", Test.class)
				.setParameter("NumeTest", NumeTest)
				.getSingleResult();
	}	
	
	// Others
	public String getMessage() {
		return "TestServiceEJB is ON... ";
	}
}





