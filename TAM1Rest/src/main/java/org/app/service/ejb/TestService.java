package org.app.service.ejb;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remote;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.entities.EntityBase;
import org.app.service.entities.Intrebare;
import org.app.service.entities.Test;;

// Implement simple CRUD Operations
@Remote
public interface TestService {

	// CREATE or UPDATE
			Test addTest(Test testToAdd);

			// DELETE
			String removeTest(Test testToDelete);

			// READ
			Test getTestByIdTest(Integer IdTest);
			Collection<Test> getTeste();
			
			// Custom READ: custom query
			Test getTestByNumeTest(String testNumeTest);
			
			// Others
			String getMessage();

			Intrebare getIntrebareById(int i);
}
