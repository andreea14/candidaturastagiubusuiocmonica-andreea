package org.app.service.ejb;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remote;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Candidat;
import org.app.service.entities.EntityBase;
import org.app.service.entities.Interviu;;

// Implement simple CRUD Operations
@Remote
public interface InterviuCandidat {
	// CREATE or UPDATE
			Interviu addInterviu(Interviu interviuToAdd);

			// DELETE
			String removeInterviu(Interviu interviuToDelete);

			// READ
			Interviu getInterviuByIdCandidat(Integer IdCandidat);
			Collection<Interviu> getInterviuri();
			
			// Custom READ: custom query
			Interviu getInterviuByRezultatInterviu(String interviuRezultatInterviu);
			
			// Others
			String getMessage();

			Candidat getCandidatById(Integer IdCandidat);

			
		}



