package org.app.service.ejb;

import java.util.Collection;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Candidat;
import org.app.service.entities.Interviu;
import org.app.service.entities.Intrebare;

@Remote

public interface CandidatService extends EntityRepository<Candidat> {

	// Classic CRUD methods
		// CREATE and UPDATE
		 Candidat addCandidat(Candidat candidatToAdd);				//inherited from EntityRepository
		// READ
		Candidat getCandidatByIdCandidat(Integer IdCandidat);			//	inherited from EntityRepository*/
		 Collection<Candidat> getCandidati();	//inherited from EntityRepository*/
		// DELETE
		String removeCandidat(Candidat candidatToDelete); //inherited from EntityRepository*/
		
		// Custom READ: custom query
		Candidat getCandidatByNumeCandidat(String candidatNumeCandidat);

		// Other operations
		String getMessage();
		String saveCandidat(Candidat candidatToSave);
}
