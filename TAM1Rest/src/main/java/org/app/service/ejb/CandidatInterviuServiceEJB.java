package org.app.service.ejb;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.Candidat;
import org.app.service.entities.Interviu;
import java.util.logging.Logger;


@Stateless @LocalBean
public class CandidatInterviuServiceEJB extends EntityRepositoryBase<Candidat> implements CandidatInterviuService, Serializable {

	private static Logger logger = Logger.getLogger(CandidatInterviuServiceEJB.class.getName());
	
	@EJB
	private InterviuCandidat interviuCandidat;
	private EntityRepository<Interviu> interviuRepository;
	
	@PostConstruct
public void init() {
		interviuRepository= new EntityRepositoryBase<Interviu>(this.em,Interviu.class);
		logger.info("POSTCONSTRUCT-INIT interviuRepository: " + this.interviuRepository);
		logger.info("POSTCONSTRUCT-INIT interviuCandidat: " + this.interviuCandidat);}
public Candidat createNewCandidat(Integer IdCandidat, String Telefon, String Email){
	Candidat candidat = new Candidat(IdCandidat, "new Candidat" + "." + IdCandidat, Telefon, Email );
	List<Interviu> interviuriCandidat=new ArrayList<>();
	Integer interviuCount=3;
	Date dataInterviu= new Date(interviuCount);
	Long interval= (long) (301 /*zile*/ * 24 /*ore*/ * 60 /*min*/ * 60 /*sec*/ * 1000 /*milisec*/);
	for(int i=0; i<= interviuCount-1;i++){
	interviuriCandidat.add(new Interviu(i, new Date(dataInterviu.getTime() + i*interval),"NumeIntervievator" + i, 100+i ,candidat));
	}
	candidat.setInterviuri(interviuriCandidat);
	this.add(candidat);
	return candidat;
	}
public Interviu getInterviuById(Integer IdInterviu){
	return interviuRepository.getById(IdInterviu);
}
public String getMessage(){
	return "CandidatIntrebare Data Service is working..";
}
@Override
public Collection<Interviu> getInterviuri() {
	// TODO Auto-generated method stub
	return null;
}
}
