package org.app.service.ejb;



import java.util.Collection;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Candidat;
import org.app.service.entities.Interviu;

public interface CandidatInterviuService extends EntityRepository<Candidat> {
	Candidat createNewCandidat(Integer IdCandidat, String Telefon, String Email);
	Interviu getInterviuById(Integer IdInterviu);
	 Collection<Interviu> getInterviuri();
	String getMessage();
		
	
}
