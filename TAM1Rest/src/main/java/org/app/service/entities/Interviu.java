package org.app.service.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Interviu implements  Serializable{
	@Id
	private Integer IdInterviu;
		@Temporal (TemporalType.DATE)
	private Date DataInterviu;

	private String NumeIntervievator;
	private String RezultatInterviu;
	@ManyToOne
	private Candidat candidat;
	@OneToMany(fetch=FetchType.EAGER)
	private List<Test> teste=new ArrayList<>();

	public Interviu(Integer idInterviu, Date dataInterviu, String numeIntervievator, String rezultatInterviu,
			Candidat candidat, List<Test> teste) {
		super();
		IdInterviu = idInterviu;
		DataInterviu = dataInterviu;
		NumeIntervievator = numeIntervievator;
		RezultatInterviu = rezultatInterviu;
		this.candidat = candidat;
		this.teste = teste;
	}

	public Integer getIdInterviu() {
		return IdInterviu;
	}

	public void setIdInterviu(Integer idInterviu) {
		IdInterviu = idInterviu;
	}

	public Date getDataInterviu() {
		return DataInterviu;
	}

	public void setDataInterviu(Date dataInterviu) {
		DataInterviu = dataInterviu;
	}

	public String getNumeIntervievator() {
		return NumeIntervievator;
	}

	public void setNumeIntervievator(String numeIntervievator) {
		NumeIntervievator = numeIntervievator;
	}

	public String getRezultatInterviu() {
		return RezultatInterviu;
	}

	public void setRezultatInterviu(String rezultatInterviu) {
		RezultatInterviu = rezultatInterviu;
	}

	public Candidat getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public List<Test> getTeste() {
		return teste;
	}

	public void setTeste(List<Test> teste) {
		this.teste = teste;
	}

	public Interviu() {
		super();
	}

	public Interviu(Integer idInterviu, Date dataInterviu, String numeIntervievator, String rezultatInterviu,
			Candidat candidat) {
		super();
		IdInterviu = idInterviu;
		DataInterviu = dataInterviu;
		NumeIntervievator = numeIntervievator;
		RezultatInterviu = rezultatInterviu;
		this.candidat = candidat;
	}

	public Interviu(int i, java.sql.Date date, String string, int j, Candidat candidat2) {
		// TODO Auto-generated constructor stub
	}

	public Interviu(int i, Date date, String string, String string2, Integer idCandidat) {
		// TODO Auto-generated constructor stub
	}

	public Interviu(int i, Date date, String string, String string2, Interviu candidat2) {
		// TODO Auto-generated constructor stub
	}


	}



