package org.app.service.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity

public class Test implements  Serializable {
	@Id
	private Integer IdTest;
	private String NumeTest;
	@Temporal (TemporalType.DATE)
	private Date DataTest;
	private Integer DurataTest;
	private Integer RezultatTest;
	
	@OneToMany()
	private List<Intrebare> intrebari=new ArrayList<>();

	public Test(Integer idTest, String numeTest, Date dataTest, Integer durataTest, Integer rezultatTest,
			List<Intrebare> intrebari) {
		super();
		IdTest = idTest;
		NumeTest = numeTest;
		DataTest = dataTest;
		DurataTest = durataTest;
		RezultatTest = rezultatTest;
		this.intrebari = intrebari;
	}

	public Integer getIdTest() {
		return IdTest;
	}

	public void setIdTest(Integer idTest) {
		IdTest = idTest;
	}

	public String getNumeTest() {
		return NumeTest;
	}

	public void setNumeTest(String numeTest) {
		NumeTest = numeTest;
	}

	public Date getDataTest() {
		return DataTest;
	}

	public void setDataTest(Date dataTest) {
		DataTest = dataTest;
	}

	public Integer getDurataTest() {
		return DurataTest;
	}

	public void setDurataTest(Integer durataTest) {
		DurataTest = durataTest;
	}

	public Integer getRezultatTest() {
		return RezultatTest;
	}

	public void setRezultatTest(Integer rezultatTest) {
		RezultatTest = rezultatTest;
	}

	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}

	public Test() {
		super();
	}

	public Test(int idTest2, String numeTest2, Date dataTest2, int durataTest2, int rezultatTest2,
			Intrebare intrebare) {
		// TODO Auto-generated constructor stub
	}
	
	
}

