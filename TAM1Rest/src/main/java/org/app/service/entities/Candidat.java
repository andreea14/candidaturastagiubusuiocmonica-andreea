package org.app.service.entities;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity

public class Candidat  implements  Serializable{
	
	@Id
	private Integer IdCandidat;
	private String NumeCandidat;
	private String Telefon;
	private String Email;
	
	
	public Candidat(Integer idCandidat, String numeCandidat, String telefon, String email) {
		super();
		IdCandidat = idCandidat;
		NumeCandidat = numeCandidat;
		Telefon = telefon;
		Email = email;
		
	}
	
	public  Integer getIdCandidat() {
		return IdCandidat;
	}
	public void setIdCandidat(Integer idCandidat) {
		IdCandidat = idCandidat;
	}
	public String getNumeCandidat() {
		return NumeCandidat;
	}
	public void setNumeCandidat(String numeCandidat) {
		NumeCandidat = numeCandidat;
	}
	public String getTelefon() {
		return Telefon;
	}
	public void setTelefon(String telefon) {
		Telefon = telefon;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}

	public Candidat() {
		super();
	}

	public void setInterviuri(List<Interviu> interviuriCandidat) {
		// TODO Auto-generated method stub
		
	}

	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setName(String string) {
		// TODO Auto-generated method stub
		
	}

	public List<Interviu> getInterviuri() {
		// TODO Auto-generated method stub
		return null;
	

		
	}
	
	}

	

