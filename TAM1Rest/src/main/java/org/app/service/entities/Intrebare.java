package org.app.service.entities;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;

@XmlRootElement
@Entity
public class Intrebare  implements Comparable<Intrebare>, Serializable {
	@Id
	private Integer IdIntrebare;
	private String ContinutIntrebare;
	private Integer PunctajIntrebare;
	public Intrebare(Integer idIntrebare, String continutIntrebare, Integer punctajIntrebare) {
		super();
		IdIntrebare = idIntrebare;
		ContinutIntrebare = continutIntrebare;
		PunctajIntrebare = punctajIntrebare;
	}
	@XmlElement
	public Integer getIdIntrebare() {
		return IdIntrebare;
	}
	public void setIdIntrebare(Integer idIntrebare) {
		IdIntrebare = idIntrebare;
	}
	public String getContinutIntrebare() {
		return ContinutIntrebare;
	}
	public void setContinutIntrebare(String continutIntrebare) {
		ContinutIntrebare = continutIntrebare;
	}
	public Integer getPunctajIntrebare() {
		return PunctajIntrebare;
	}
	public void setPunctajIntrebare(Integer punctajIntrebare) {
		PunctajIntrebare = punctajIntrebare;
	}
	public Intrebare() {
		super();
	}
	
	@Override
	public int compareTo(Intrebare other) {
		if (this.equals(other))
			return 0;
		return this.getContinutIntrebare().compareTo(other.getContinutIntrebare());
	}

}

