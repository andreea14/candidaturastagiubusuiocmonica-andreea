var demoAppRest = angular.module('Tam1App', []);

demoAppRest.controller('candidatiController', function($scope) {
	// projects data model
    $scope.candidatList = [
{
    "idCandidat": 1,
    "numeCandidat": "Candidat 1",
    "telefon": "0756405678",
    "email": "abc@yahoo.com"
},
{
	 "idCandidat": 2,
	    "numeCandidat": "Candidat 2",
	    "telefon": "0756405675",
	    "email": "ac@yahoo.com"
},
{
	 "idCandidat": 3,
	    "numeCandidat": "Candidat 3",
	    "telefon": "0756405658",
	    "email": "a@yahoo.com"
}		
];
});